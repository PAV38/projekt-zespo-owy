﻿using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Isi2.Projekt.Common.Proxy.Service;
using Isi2.Projekt.Common.Proxy.Service.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;

namespace Isi2.Projekt.Web.Controllers
{
    public class HomeController : Controller
    {
        private static WindsorContainer container = GetContainer();
        private ICommonProxyService proxyService = container.Resolve<ICommonProxyService>();
 

        public ActionResult Index()
        {
            
            return View();
        }

        public JsonResult GetObjects(PagedListRequestFilter filter)
        {
            var result = proxyService.GetData(filter);

            return Json(result, JsonRequestBehavior.AllowGet
           );
        }

        public ActionResult About()
        {
            return View();
        }

        private static WindsorContainer GetContainer()
        {
            var container = new WindsorContainer();
            container.Kernel.AddFacility<WcfFacility>();
            container.Register(
                Component.For<ICommonProxyService>()
                    .AsWcfClient(new DefaultClientModel {
                        Endpoint = WcfEndpoint.BoundTo(new BasicHttpBinding())
                        .At("http://localhost:52073/CommonProxyService.svc")
                    }
                )
            );
            return container;
        }
    }
}
