﻿window.StandardKendoGridConfiguration = {
    dataSource: {
        type: "json",
        pageSize: 10,
        schema: {
            total: function (data) {
                return data.Count;
            },
            data: function (data) {
                return data.Items;
            },
        },
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true
    },
    groupable: false,
    sortable: {
        mode: "single",
        allowUnsort: false
    },
    toolbar: [
        { name: "create", text: "Nowy rekord" }
    ],
    pageable: {
        pageSizes: [5, 10, 15, 30, 50],
        refresh: true,
        messages: {
            display: "{0} - {1} z {2} rekordów",
            empty: "Brak rekordów do wyświetlenia",
            page: "Strona",
            of: "z {0}",
            itemsPerPage: "rekordów na stronie",
            first: "Przejdź do pierwszej strony",
            previous: "Przejdź do poprzedniej strony",
            next: "Przejdź do następnej strony",
            last: "Przejdź do ostatniej strony",
            refresh: "Odśwież"
        }
    },
};