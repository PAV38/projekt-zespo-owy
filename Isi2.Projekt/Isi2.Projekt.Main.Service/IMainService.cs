﻿using Isi2.Projekt.Common.Proxy.Service.Contracts;
using Isi2.Projekt.Main.Service.Contracts;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Isi2.Projekt.Main.Service
{
    [ServiceContract]
    public interface IMainService
    {
        [OperationContract]
        PagedListRequestContract<SampleContract> GetData(PagedListRequestFilter filter);
    }
}
