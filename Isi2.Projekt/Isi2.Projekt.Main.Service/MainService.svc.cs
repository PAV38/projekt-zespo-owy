﻿using Isi2.Projekt.Common.Proxy.Service.Contracts;
using Isi2.Projekt.Main.Service.Contracts;
using Isi2.Projekt.Main.Service.Model.Repositories;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Isi2.Projekt.Main.Service
{
    public class MainService : IMainService
    {
        private SampleRepository repository = new SampleRepository();

        public PagedListRequestContract<SampleContract> GetData(PagedListRequestFilter filter)
        {
            return repository.GetData(filter);
        }
    }
}
