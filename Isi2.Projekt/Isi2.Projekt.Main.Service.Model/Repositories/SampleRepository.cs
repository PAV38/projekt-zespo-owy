﻿using Isi2.Projekt.Common.Proxy.Service.Contracts;
using Isi2.Projekt.Main.Service.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Isi2.Projekt.Main.Service.Model.Repositories
{
    public class SampleRepository
    {
        public PagedListRequestContract<SampleContract> GetData(PagedListRequestFilter filter)
        {
            var objects = new List<SampleContract>();

            for (int i = 0; i < 100; i++)
            {
                objects.Add(new SampleContract
                {
                    Id = i,
                    Name = "Obiekt" + i
                });
            }

            var count = objects.Count;

            if (filter != null)
            {
                switch (filter.SortField)
                    {
                        case "Name":
                            objects = filter.SortOrder == "asc" ? objects.OrderBy(c => c.Name).ToList() : objects.OrderByDescending(c => c.Name).ToList();
                            break;
                        default:
                            objects = filter.SortOrder == "asc" ? objects.OrderBy(c => c.Id).ToList() : objects.OrderByDescending(c => c.Id).ToList();
                            break;
                    }

                    objects = objects.Skip(filter.PageSize * (filter.Page - 1)).Take(filter.PageSize).ToList();
            }

            return new PagedListRequestContract<SampleContract> { Items = objects, Count = count, Success = true };
        }
    }
}
