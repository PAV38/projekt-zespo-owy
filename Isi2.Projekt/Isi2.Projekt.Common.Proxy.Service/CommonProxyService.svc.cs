﻿using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Isi2.Projekt.Main.Service;
using Isi2.Projekt.Main.Service.Contracts;
using Isi2.Projekt.Common.Proxy.Service.Contracts;

namespace Isi2.Projekt.Common.Proxy.Service
{
    public class CommonProxyService : ICommonProxyService
    {
        private WindsorContainer container = GetContainer();
        private IMainService mainService;

        public CommonProxyService()
        {
            mainService = container.Resolve<IMainService>();
        }

        public PagedListRequestContract<SampleContract> GetData(PagedListRequestFilter filter)
        {
            return mainService.GetData(filter);
        }

        private static WindsorContainer GetContainer()
        {
            var container = new WindsorContainer();
            container.Kernel.AddFacility<WcfFacility>();
            container.Register(Component.For<IMainService>()
                                   .AsWcfClient(new DefaultClientModel
                                   {
                                       Endpoint = WcfEndpoint.BoundTo(new BasicHttpBinding())
                                           .At("http://localhost:52312/MainService.svc")
                                   }));
            return container;
        }
    }
}
