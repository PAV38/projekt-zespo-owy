﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.ComponentModel;

namespace Isi2.Projekt.Common.Proxy.Service.Contracts
{
    [DataContract]
    public class PagedListRequestFilter
    {
        [DataMember]
        public int Take { get; set; }

        [DataMember]
        public int Page { get; set; }

        [DataMember]
        public int PageSize { get; set; }

        [DataMember]
        public int Skip { get; set; }

        [DataMember]
        public string SortOrder { get; set; }

        [DataMember]
        public string SortField { get; set; }


        public PagedListRequestFilter()
        {
            if (HttpContext.Current != null)
            {
                HttpRequest curRequest = HttpContext.Current.Request;
                this.SortOrder = curRequest["sort[0][dir]"];
                this.SortField = curRequest["sort[0][field]"];
            }
        }
    }
}
